SRC_DIR=${CURDIR}/src

install-basex: # BaseX & dépendances: Téléchargement, installation et définition du mot de passe admin
	@if [ -d '.max/basex' ]; then\
		echo 'BaseX install OK.';\
	else\
	  	mkdir .max;\
		cd .max;\
		curl https://files.basex.org/releases/11.1/BaseX111.zip --output BaseX111.zip;\
		unzip BaseX111.zip;\
		curl https://repo1.maven.org/maven2/net/sf/saxon/Saxon-HE/10.8/Saxon-HE-10.8.jar --output Saxon-HE-10.8.jar;\
		mv Saxon-HE-10.8.jar basex/lib/custom/;\
		rm BaseX111.zip;\
		cd ..;\
		./.max/basex/bin/basexhttpstop || true;\
		./.max/basex/bin/basexhttp -S ;\
		echo 'Please define a BaseX admin password';\
		./.max/basex/bin/basex -c'PASSWORD';\
		./.max/basex/bin/basexhttpstop;\
		echo 'BaseX install done.';\
    fi
.PHONY: install-basex


install: install-basex ## Installation de MaX
	@if [ -d '.max/basex/webapp/max' ]; then\
		echo 'MaX install OK.';\
	else\
		mkdir -p .max/basex/webapp/max;\
	  	mkdir -p .max/basex/repo/max;\
		ln -s "$(SRC_DIR)/main/webapp/routes.xqm" .max/basex/webapp/max/routes.xqm;\
		ln -s "$(SRC_DIR)/resources" .max/resources;\
		ln -s ../../../../expath-pkg.xml .max/basex/repo/max/expath-pkg.xml;\
		ln -s "$(SRC_DIR)/main/core" .max/basex/repo/max/max;\
		ln -s ../fixtures .max/fixtures;\
		echo 'MaX install done.';\
	fi
.PHONY: install

package: install ## Construction d'une version distribuable de MaX
	@if [ -d package ]; then\
		rm -rf package;\
	fi
	@mkdir -p package
	@cp -rL .max package/.max
# nettoyage du BaseX copié : suppression fichiers liées aux bundles, des données
	@rm package/.max/basex/.basex
	@rm package/.max/basex/webapp/max/*.xqm
	@rm -rf package/.max/basex/repo/max-*
	@find package/.max/basex/data/ -mindepth 1 -type d -exec rm -rf '{}' '+'
# copie des sources à la racine du .max
	@cp $(SRC_DIR)/main/webapp/routes.xqm package/.max/basex/webapp/max/
	@mkdir package/.max/src
	@cp -r src/main package/.max/src/
# génération d'un fichier VERSION
	@git rev-parse HEAD >> package/VERSION
	@echo 'Package built in package directory.'
# création du Makefile (les cibles de dev sont supprimées)
	@echo -n "SRC_DIR=$$" > package/Makefile
	@echo "{CURDIR}/.max/src" >> package/Makefile
	@awk '/.PHONY: package/ {p=1;next}p' Makefile >> package/Makefile
.PHONY: package

demo:  ## Installation d'une édition de démo
	@if [ ! -d '.max/basex' ]; then\
		echo 'MaX is not installed, please run make install first.';\
	else\
		cp -r .max/fixtures/max .max/basex/data/;\
		cp -r .max/fixtures/content_html .;\
		cp -r .max/fixtures/templates .;\
		cp -r .max/fixtures/autoroute .;\
		cp -r .max/fixtures/config.xml .;\
		ln -s "$(SRC_DIR)/main/bundles/max-tei-bundle" .max/basex/repo/max-tei-bundle;\
		echo 'demo installed.';\
	fi
.PHONY: demo

check: ## vérification de la configuration
	@if [ -f 'config.xml' ]; then\
		.max/basex/bin/basex -q"validate:rng('config.xml', '.max/resources/max-configuration.rng')" || false;\
		echo 'SUCCESS : config.xml file is valid.';\
	else\
  		echo 'Missing configuration file : config.xml';\
  		exit 1;\
  	fi
.PHONY: check

run:  check ## Lancement de MaX
	@.max/basex/bin/basexhttpstop || true
	@.max/basex/bin/basexhttp -h1234
.PHONY: run

build:	run ## Construction de la version statique dans le répertoire dist/
	@if [ -d dist ]; then\
		rm -rf dist;\
	fi
	sleep 5
	@echo "Statification..." ; \
	wget -P dist -nH --mirror --page-requisites --html-extension --convert-links 'http://127.0.0.1:1234/fr/pages/index.html' ; \
	echo "Version statique construite dans le répertoire dist/"
.PHONY: build

clean: ## Supprime l'installation de MaX et les fichiers du projet
	@read -p "Êtes-vous sûr? [o/N] " ans && ans=$${ans:-N} ; \
    if [ $${ans} = o ] || [ $${ans} = O ]; then \
		rm -rf content_html;\
		rm -rf templates;\
		rm -rf autoroute;\
		rm config.xml;\
		rm -rf .max;\
    else \
        echo "Opération annulée" ; \
    fi
.PHONY: clean

help: ## Affiche cette aide
	@echo "\nChoisissez une commande. Les choix sont:\n"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[0;36m%-12s\033[m %s\n", $$1, $$2}'
	@echo ""
.PHONY: help

test:
	@echo $(SRC_DIR)
