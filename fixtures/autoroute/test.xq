xquery version "3.0";

import module namespace templating = 'https://certic.unicaen.fr/max/templating';

declare variable $lang  external;

let $translations := map {
    'fr' : "Ceci est un test d'autoroute !",
    'en' : 'This is a custom autoroute test !'
}

let $content := <div>
                    <p>{$translations?$lang} (<code>autoroute/test.xq</code>)</p>
                    {
                        for $n in 1 to 42
                        return <span class="me-2">{$n}</span>
                    }
                    <p></p>
                </div>
return templating:render('page.html', map{'content': $content, 'lang': $lang})