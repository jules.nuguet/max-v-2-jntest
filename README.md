# MaX v2

## Prérequis
 - Java

## Dépendances
 - BaseX
 - Saxon

Celles-ci sont automatiquement installées par la commande `make install`

## Installation

``
make help
``

## Fonctionnement général

L'application est scindée en 2 blocs :
 - le cœur : déclaration des routes/fonctionnalités de base, accès aux sources xml et à la configuration, système de templating, traduction etc.
 - les bundles : chaque bundle ajoute une (ou des) fonctionnalité à MaX.

Il existe actuellement 3 bundles en cours de développement :
 - max-tei-bundle : bundle de gestion de l'affichage de sources encodées selon le vocabulaire XML-TEI.
 - max-contents-html-bundle : affichage de fichiers statiques HTML stockés dans le répertoire `content_html`.
 - max-autoroute-bundle : exécution d'une xquery dont la route correspond à un fichier du même nom.

Les sources XML à afficher doivent être stockées dans une base nommée `max`.

## Bundle de vocabulaire
    
Un bundle de *vocabulaire* est spécialisé dans le rendu d'un vocabulaire XML. C'est le cas du `max-tei-bundle`.
Ce type de bundle doit contenir :
 - une fonction xquery déclarée dans l'espace de nom du bundle et définie par `[ns]:doc-to-html($document as document-node(), $lang as xs:string)`
  Cette fonction sera automatiquement appelée par le cœur pour la route `/{$lang=[a-z]{2}}/{$collection=.+}/{$doc=[a-zA-Z0-9_]+}.html`
 - un dossier templates contenant les templates :
   - `page.html` : template appliqué par défaut par le cœur pour les rendus des différentes pages
   - `error.html` : template d'erreur appliqué par défaut par le cœur
 - un fichier `expath-pkg.xml` définissant le module au format EXPath
 - un dossier `autoroute` (optionnel) avec un fichier `[page].xq` pour chaque fonctionnalité servie sur la route `/{$lang=[a-z]{2}}/{$page=[a-zA-Z0-9_]+}.html`
 - un dossier `webapp` (optionnel) contenant les routes (restxq) servies par le bundle
 - un dossier `locales` (optionnel) avec un fichier `[codelang].json` par langues gérées
 - un dossier `static` (optionnel) contenant les assets du bundle (fichiers js, css, images etc.)


### Configuration

La configuration se fait dans le fichier `config.xml`.

Afin d'opérer à un rendu, un bundle de vocabulaire doit être déclaré dans le fichier de configuration de MaX.

Exemple de configuration minimale :

````xml
<configuration xmlns="http://certic.unicaen.fr/max/ns/1.0" env="dev" vocabulary-bundle="max-tei-bundle">
    <languages>
        <language>fr</language>
        <language>en</language>
    </languages>
    <title>mon Corpus Numérique</title>
</configuration>
````


### Modules XQuery
Le cœur et les bundles sont packagés puis déclarés comme modules XQUERY ([EXPath Packaging](https://docs.basex.org/12/Repository#expath_packaging)) auprès de BaseX afin de faciliter
le développement.


## Organisation des sources

- `src` : toutes les sources
  - `main` : sources applicatives
    - `core`
    - `bundles`
    - `webapp`
  - `resources` : ressources additionnelles  
  - `test` : source des tests

## Le dossier `.max`

Il contient le serveur BaseX. C'est un dossier (caché) purement technique dans lequel l'utilisateur n'aura pas (et ne devra pas) intervenir.



## Liens symboliques dans un environnement de développement

- dossier `max` dans`.max/basex/webapp/max/` avec lien vers  `src/main/webapp/routes.xqm` et les différents fichiers RestXQ des bundles `src/main/bundles/*/webapp/*.xqm`
- dossier max dans `.max/basex/repo` avec lien vers `src/main/core` et `expath-pkg.xml`
- liens depuis `.max/basex/repo` vers les différents bundles (`src/main/bundles`)

Ces liens sont automatiquement créé par la commande `make install`

*Note* : 
Tous ces liens doivent être gérés par le CLI et/ou le Makefile, le développeur de MaX ou d'un Bundle n'édite que les fichiers dans `src/`.
L'utilisateur édite dans : `/templates`, `/autoroute`, `/content_html`.
Dans la version packagée, les liens symboliques seront remplacés par les fichiers (absence du dossier `src`)

## Les routes principales

### Du cœur

Elles sont déclarées dans le fichier `src/main/routes.xqm`

- `/{$lang=[a-z]{2}}/{$collection=.+}/{$doc=[a-zA-Z0-9_]+}.html` : version HTML du source XML nommé `$doc.xml` dans la collection `$collection`
- `/{$bundle}/static/{$filename}` : fichier statique d'un bundle (js, css, etc.)
- `/sources.zip` : archive zip contenant les sources XML
- `/max-infos.html` : affiche les informations de configuration et d'environnement. (disponible uniquement en mode `dev`)

### Des bundles

#### max-content-html-bundle

`/{$lang=[a-z]{2}}/pages/{$page=[a-zA-Z0-9_]+}.html` : rend la page HTML stockée dans  `content_html/[$lang]/[$page].html`


#### max-autoroute-bundle
`/{$lang=[a-z]{2}}/{$page=[a-zA-Z0-9_]+}.html` : exécute 

- `autoroute/{$page}.xq` : si le fichier existe
- sinon, `.max/basex/repo/${bundle_vocabulaire}/autoroute/{$page}.xq` : si le fichier existe
- sinon, exécute la fonction `${bundle_vocabulaire}:doc-to-html` du bundle de vocabulaire actif (transformation de la source `{$page}.xml`)

Par exemple, si `max-tei-bundle` est actif, la route `/fr/tdm.html` retourne le résultat de `.max/basex/repo/max-tei-bundle/autoroute/tdm.xq`


*Question/remarque* : les fonctionnalités de `max-autoroute-bundle` ne devraient-elle pas être intégrées au cœur ?

## Templating

@todo

## I18n

@todo

