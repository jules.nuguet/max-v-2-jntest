xquery version "3.0";

(:~
: MaX "TEI" bundle
:)
module namespace tei = "https://certic.unicaen.fr/max/bundles/tei";

import module namespace templating = "https://certic.unicaen.fr/max/templating";
import module namespace database = "https://certic.unicaen.fr/max/database";

declare function tei:doc-to-html($document as document-node(), $lang as xs:string){
    let $content := <div>
                        <h2>TEI Brute</h2>
                        <h3>TODO : transform tei {fn:base-uri($document)} to html - lang = {$lang}</h3>
                        {$document}
                    </div>
    return templating:render('page.html', map {'content': $content, 'lang': $lang})
};