xquery version "3.0";

import module namespace database = 'https://certic.unicaen.fr/max/database';
import module namespace templating = 'https://certic.unicaen.fr/max/templating';
import module namespace utils = 'https://certic.unicaen.fr/max/utils';

declare variable $lang  external;

let $tdm :=
    <div>
        <h4>{templating:translate('tdm',$lang)}</h4>
        <ul>{
        for $doc in collection($database:max-db)
            return <li><a href="{utils:get-document-route($doc, $lang)}">{$doc//*:title/text()}</a></li>
            }
        </ul>
        <p>
            Générée par <code>max-tei-bundle/autoroute/tdm.xq</code>
        </p>
    </div>

return
    templating:render('page.html', map{'content': $tdm, 'lang': $lang})
