xquery version "3.0";

(:~
: MaX routes
:)
module namespace routes = 'https://certic.unicaen.fr/max/routes';

import module namespace conf = 'https://certic.unicaen.fr/max/conf';
import module namespace database = 'https://certic.unicaen.fr/max/database';
import module namespace templating = 'https://certic.unicaen.fr/max/templating';
import module namespace errors = 'https://certic.unicaen.fr/max/errors';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n';
import module namespace utils = 'https://certic.unicaen.fr/max/utils';
import module namespace globals = 'https://certic.unicaen.fr/max/globals';
import module namespace markdown = 'https://certic.unicaen.fr/max/markdown';

(:~
 : @return default html index page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path("/{$path=(index.html)?")
function routes:index($path as xs:string?){
   routes:page($conf:default-lang, 'index')
};


(:~
 : @param $lang language
 : @param $collection collection
 : @param $doc xml filename
 : @return html data
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/{$collection=.+}/{$doc=[a-zA-Z0-9_]+}.html")
function routes:document($lang as xs:string, $collection as xs:string?, $doc as xs:string){
    try{
        utils:doc-to-html($collection, $doc, $lang)
    }
    catch err:max-database{errors:error($err:code, $err:description, $err:line-number, $lang)}
};

(:~
 : Returns content page
 : @param $lang language
 : @param  $page  file or unknown path
 : @return html data
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/pages/{$path=.+}.html")
function routes:page($lang as xs:string, $path as xs:string){
        try {
            let $mdFile := file:current-dir() || $globals:content-html-dir || '/' || $lang || '/' || $path || '.md'
            let $htmlFile := file:current-dir() || $globals:content-html-dir || '/' || $lang || '/' || $path || '.html'
            let $content := if(file:exists($mdFile))
                            then markdown:parse(file:read-text($mdFile))
                            else html:doc($htmlFile)

          return templating:render('page.html', map{'content' : $content, 'lang': $lang})
        }
        catch err:FODC0002 {errors:not-found($err:code || ' - ' ||$err:description, $lang)}
        catch err:max-templating {errors:simple-error($err:code, $err:description, $err:line-number)}
        catch err:max-conf {errors:simple-error($err:code, $err:description, $err:line-number)}
};


(:~
 : @param $lang language
 : @param $page xq filepath or document path
 : @return html xq filepath result if exists, else try to transform $page document as HTML
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/{$lang=[a-z]{2}}/{$page=[a-zA-Z0-9_]+}.html")
function routes:autoroute($lang as xs:string, $page as xs:string){
    try {
        let $xqueryFile := utils:get-autoroute-file-path($page || '.xq')
        return xquery:eval(xs:anyURI($xqueryFile), map {'lang': $lang})
    }
    catch * {
            try {
                let $content := utils:doc-to-html((), $page, $lang)
                return $content
            }
            catch * { errors:errors(
                ('no ' || $page || '.xq file', ' no ' || $page || '.xml document',  $err:description), $lang)}
    }
};


(:~
 : @return xml sources as zip file
 :)
declare
%rest:GET
%output:media-type("application/octet-stream")
%rest:query-param("indent", "{$indent}", 'false')
%rest:path("/sources.zip")
function routes:sources-export($indent){
    try{
        let $zipName := 'sources.zip'
        let $tmpDir := file:create-temp-dir('max-export', $database:max-db)
        let $dest := $tmpDir ||'/'||$zipName
        return (
               db:export($database:max-db, $tmpDir, map { 'indent': if($indent='true') then true() else false()}),
               utils:make-zip($tmpDir, $dest),
                <rest:response>
                    <http:response status="200">
                        <http:header name="content-type" value="application/zip"/>
                        <http:header name="Content-Disposition" value="attachment; filename={$zipName}"/>
                    </http:response>
                </rest:response>,
                file:read-binary($dest)
                )
    }
    catch * {$err:description}
};

declare %private function routes:configuration-as-html(){
    let $config := conf:get-configuration()
    return
        <section>
            <h2>Configuration (config.xml)</h2>
            <ul>
                <li><strong>vocabulaire : </strong>{conf:get-vocabulary-bundle()} ({conf:get-vocabulary-namespace()})</li>
                <li><strong>langues : </strong>{fn:string-join(conf:get-languages(),', ')} </li>
            </ul>
        </section>
};


(:~
 : @return html max info page
 :)
declare
%rest:GET
%output:method("html")
%output:html-version('5')
%rest:path("/max-infos.html")
function routes:max-infos(){
    if(conf:is-dev-mode())
    then
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta charset="UTF-8"/>
            <title>MaX - infos</title>
        </head>
        <body>

            <h1> <a href="/{$conf:default-lang}/pages/index.html">MaX</a></h1>
            <h2>Version <code>{conf:get-max-version()}</code></h2>

            <section>
                <h2>Base de données XML <strong>{$database:max-db} </strong></h2>
                    {if(db:exists($database:max-db))
                        then
                            <div>
                                <ul>{
                                for $d in db:list($database:max-db)
                                return <li>{$d}</li>
                                }</ul>
                            </div>
                        else <span style='color:red;'> NOK</span>
                    }
            </section>
            {routes:configuration-as-html()}
            <section>
                <h2>Contexte</h2>
                <ul>
                    <li><strong>file:current-dir </strong> {file:current-dir()}</li>
                    <li><strong>file:base-dir </strong> {file:base-dir()}</li>
                </ul>
            </section>
            <section>
                <h2>Locales</h2>
                <ul>
                 {for $p in i18n:getLocalePaths()
                    return <li>{$p}</li>
                 }
                 </ul>
            </section>
            <section>
                <h2>Routes</h2>
                {routes:list()}
            </section>
            <section>
                <h2>Modules</h2>
                <ul>
                {
                    for $package in repo:list()
                    return <li>{$package}<strong>{string($package/@*:name)}</strong> : {string($package/@*:version)}</li>
                }
                </ul>
            </section>
        </body>
    </html>
    else errors:not-found('', $conf:default-lang)
};

declare %private function routes:list(){
    let $wadl:=rest:wadl()
    let $maxRoutes := <ul>{
        for $r in $wadl//*:resource[@*:path]
        return
            let $path:= string($r/@*:path)
            return if(fn:contains($path,'max') or fn:contains($path,'$lang=') )
                   then <li>{$path}</li>
                   else()
     }</ul>
    return $maxRoutes

};


(:~
 : Returns a static file.
 : @param  $file  file or unknown path
 : @return rest binary data
 :)
declare %rest:path("/{$bundle}/static/{$filename}")
function routes:file(
  $bundle as xs:string, $filename as xs:string
) as item()+ {
    try {
    let $path := utils:get-bundles-path() || $bundle || '/static/' || $filename
    return (
      web:response-header(
        { 'media-type': web:content-type($path) },
        { 'Cache-Control': 'max-age=3600,public', 'Content-Length': file:size($path) }
      ),
      file:read-binary($path)
    )
    }
    catch file:not-found {web:error(404, "The requested resource cannot be found.")}

};

(:~
 :TEST
 : @return max routes xqdoc
 :)
declare
%rest:GET
%output:method("xml")
%rest:path("/max-documentation.xml")
function routes:documentation(){
    inspect:xqdoc(db:option('webpath') || '/max/routes.xqm')
};



