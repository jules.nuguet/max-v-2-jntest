xquery version "3.0";
(:~
: Global variables
:)
module namespace globals = 'https://certic.unicaen.fr/max/globals';

(:~ locales dir:)
declare variable $globals:locales-dir := "locales/";
(:~ contents_html dir:)
declare variable $globals:content-html-dir := "content_html";
(:~ autoroute dir:)
declare variable $globals:autoroute-dir := "autoroute";