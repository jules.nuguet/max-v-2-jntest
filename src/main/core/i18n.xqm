xquery version "3.0";
(:~
: "i18n" module
:)
module namespace i18n = 'https://certic.unicaen.fr/max/i18n';

import module namespace conf = 'https://certic.unicaen.fr/max/conf';
import module namespace utils = 'https://certic.unicaen.fr/max/utils';
(:~
 : Returns custom translation (from locales/)
 : @return string
 :)
declare function i18n:translate ($key as xs:string, $lang as xs:string) as xs:string {
   try {
    let $translations := json:doc(i18n:getUserLocalesPath() || $lang || '.json')
    return string(($translations/json/*[local-name(.)=$key])[1]/text())
   }catch * {i18n:default-translate($key, $lang)}
};

(:~
 : Returns default translation (from vocabulary bundle locales)
 : @return string
 :)
declare function i18n:default-translate ($key as xs:string, $lang as xs:string) as xs:string {
   try {
    let $translations := json:doc(i18n:getVocabularyLocalesPath()  || $lang || '.json')
    return string(($translations/json/*[local-name(.)=$key])[1]/text())
   }catch * {$key}
};

declare function i18n:getUserLocalesPath(){
    file:current-dir() || 'locales/'
};

declare function i18n:getVocabularyLocalesPath(){
    let $voc := conf:get-vocabulary-bundle()
    let $repoPath := utils:get-bundles-path()
    return $repoPath || $voc || '/locales/'
};

declare function i18n:getLocalePaths(){
    (i18n:getUserLocalesPath(), i18n:getVocabularyLocalesPath())
};

