xquery version "3.0";

(:~
: MaX "http errors" module
:)
module namespace errors = 'https://certic.unicaen.fr/max/errors';
import module namespace templating = 'https://certic.unicaen.fr/max/templating';
import module namespace conf = 'https://certic.unicaen.fr/max/conf';

(:~
 : Erreur 404 - page non trouvée
 : Cherche à utiliser le template error.html du bundle de vocabulaire, sinon le message brut est renvoyé
 : @return error page
 :)
declare
%output:method("html")
%output:html-version('5')
function errors:not-found($message, $lang as xs:string) {
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode()) then $message else ''
  let $render := if($tmpl)
    then templating:render('error.html', map{'title': templating:translate('not-found', $lang),'content' : $detail, 'lang' : $lang})
    else $message
  return(
  <rest:response>
    <http:response status="404">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log($message),
  $render)

};


declare
%output:method("html")
%output:html-version('5')
function errors:simple-error($code as xs:QName, $desc as xs:string, $line as xs:integer) {
  let $message := string-join(($code,  $desc, 'line ' || $line),', ')
  return
  (<rest:response>
    <http:response status="500">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log($message),
  'MaX Error : ' || $message)
};


declare
%output:method("html")
%output:html-version('5')
function errors:error($code as xs:QName, $desc as xs:string, $line as xs:integer, $lang as xs:string) {
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode()) then string-join(($code,  $desc, 'line ' || $line),', ') else ''
  let $render := templating:render(
    'error.html',
    map{'title': templating:translate('server-error', $lang),'content' : $detail, 'lang' : $lang})
  return
  (<rest:response>
    <http:response status="500">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log($detail),
  $render)
};


declare
%output:method("html")
%output:html-version('5')
function errors:errors($messages, $lang as xs:string?) {
  let $tmpl := templating:template-file('error.html')
  let $detail := if(conf:is-dev-mode())
    then  <ul>{
        for $m in $messages
        return <li>{$m}</li>
        }</ul>
    else ''
  let $render := if($tmpl)
    then templating:render(
    'error.html',
    map{'title': templating:translate('server-error', $lang),'content' : $detail, 'lang' : $lang})
    else fn:string-join($messages, ' / ')
  return(
  <rest:response>
    <http:response status="500">
      <http:header name="Content-Language" value="en"/>
      <http:header name="Content-Type" value="text/html; charset=utf-8"/>
    </http:response>
  </rest:response>,
  admin:write-log(fn:string-join($messages, ' / ')),
  $render)
};