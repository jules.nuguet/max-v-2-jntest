xquery version "3.0";

(:~
: MaX "utils" module
:)
module namespace utils = "https://certic.unicaen.fr/max/utils";

import module namespace conf = 'https://certic.unicaen.fr/max/conf';
import module namespace database = 'https://certic.unicaen.fr/max/database';
import module namespace errors = 'https://certic.unicaen.fr/max/errors';
import module namespace globals = 'https://certic.unicaen.fr/max/globals';


(:~
 : @param $collection collection
 : @param $doc xml filename
 : @param $lang language
 : @return result of bundle doc-to-html
 :)
declare function utils:doc-to-html($collection as xs:string?, $doc as xs:string, $lang as xs:string){
    (:
     - exécute la fonction doc-to-html du bundle de vocabulaire actif
    :)
    try{
        let $ns := conf:get-vocabulary-namespace()
        let $declaration := "declare variable $doc external; declare variable $lang external;"
        let $import := "import module namespace vocabulary = '"|| $ns ||"';"
        let $documentPath := if ($collection) then $collection || '/' || $doc || '.xml' else $doc || '.xml'
        let $doc := database:get-document($collection || '/' || $doc || '.xml')
        let $query := 'vocabulary:doc-to-html($doc, $lang)'
        return xquery:eval($import ||  $declaration || $query, map {'doc' : $doc, 'lang': $lang})
    }
    catch repo:not-found {errors:error($err:code, $err:description, $err:line-number, $lang)}
};


(:~
 : @return max bundles path
 :)
declare function utils:get-bundles-path() as xs:string{
     db:option('repopath') || '/'
};


(:~
 : @param $filename filename
 : @return autoroute file path
 :)
declare function utils:get-autoroute-file-path($filename as xs:string){
    let $userFile := file:current-dir() || $globals:autoroute-dir || '/' || $filename
    return if(file:exists($userFile))
           then $userFile
           else
            let $voc := conf:get-vocabulary-bundle()
            let $bundleFile:= utils:get-bundles-path()  || $voc || '/' || $globals:autoroute-dir || '/' || $filename
            return if(file:exists($bundleFile))
                   then $bundleFile
                    else fn:error(xs:QName('err:autoroute'),'no autoroute file found')
};



(:~
 : @return max locales path
 :)
declare function utils:get-locales-path() as xs:string{
    file:base-dir() || $globals:locales-dir
};

(:~
 : Returns consult route for an xml doc
 : @param $document document node
 : @param $lang language
 : @return string document route
 :)
declare function utils:get-document-route($document as document-node(), $lang as xs:string) as xs:string {
    '/' || $lang || fn:replace(fn:replace(base-uri($document),$database:max-db || '/',''),'.xml','.html')
};

declare function utils:make-zip($directory, $dest){
    let $files := file:list($directory, true(), '*.xml')
    let $zip   := archive:create($files,
            for $file in $files
            return file:read-binary($directory || $file)
    )
    return file:write-binary($dest, $zip)
};

declare function utils:bundle-exists($bundleName as xs:string) as xs:boolean{
    file:exists(utils:get-bundles-path() || $bundleName)
};









