xquery version "3.0";

(:~
: MaX "conf" module
: Reads configuration from a config.xml file
:)
module namespace conf = 'https://certic.unicaen.fr/max/conf';

import module namespace utils = 'https://certic.unicaen.fr/max/utils';

declare namespace max="http://certic.unicaen.fr/max/ns/1.0";
declare namespace expath="http://expath.org/ns/pkg";

(: =============== GLOBAL VARS =============== :)

(:~ config file name:)
declare variable $conf:config-file := 'config.xml';

(:~ package file name:)
declare variable $conf:package-file := 'expath-pkg.xml';

(:~ default lang:)
declare variable $conf:default-lang := 'fr';

(:~ dev mode name:)
declare variable $conf:dev-mode := 'dev';
(: =========================================== :)


(:~
 : @return config as xml
 :)
declare function conf:get-configuration() as element(){
	    doc(file:current-dir() || $conf:config-file)/max:configuration
};

(:~
 : @return config file path
 :)
declare function conf:get-configuration-path() as xs:string{
	file:current-dir() || $conf:config-file
};

(:~
 : @return MaX expath package path
 :)
declare function conf:get-package-path() as xs:string{
    db:option('repopath') || '/max/' || $conf:package-file
};

(:~
 : @return vocabulary name
 :)
declare function conf:get-vocabulary-bundle() as xs:string{
	let $bundle := string(conf:get-configuration()/@vocabulary-bundle)
	return
	if(not($bundle))
	    then fn:error(xs:QName('err:max-conf'), 'No vocabulary bundle was found in your config.xml file')
	else
	    if(not(utils:bundle-exists($bundle)))
	    then fn:error(xs:QName('err:max-conf'), 'Bundle not found : ' || $bundle ||  ' in ' || utils:get-bundles-path())
	    else $bundle
};

(:~
 : @return language list
 :)
declare function conf:get-languages() as xs:string*{
	conf:get-configuration()//max:languages/max:language/text()
};


(:~
 : @return vocabulary name
 :)
declare function conf:get-title() as xs:string*{
	string(conf:get-configuration()/max:title)
};

(:~
 : @return vocabulary name
 :)
declare function conf:is-dev-mode() as xs:boolean{
	string(conf:get-configuration()/@env) = $conf:dev-mode
};



(:~
 : @return vocabulary bundle namespace
 :)
declare function conf:get-vocabulary-namespace(){
    let $bundleName := conf:get-vocabulary-bundle()
    let $pkgFile := utils:get-bundles-path() || $bundleName || '/' || $conf:package-file
    return doc($pkgFile)//expath:xquery/expath:namespace/text()
};

(:~
 : @return MaX version
 :)
declare function conf:get-max-version(){
    let $version := string(doc(conf:get-package-path())/expath:package/@version)
    return
        if(conf:is-dev-mode())
        then
            try {
            let $commit:= proc:system('git', ('rev-parse', 'HEAD'))
            return $version || ' - commit ' ||$commit
            }
            catch * {
               $version
            }
        else $version
};




