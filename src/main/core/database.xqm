xquery version "3.0";

(:~
: MaX "database" module
:)
module namespace database = 'https://certic.unicaen.fr/max/database';

(: =============== GLOBAL VARS =============== :)
(:~ MaX db name:)
declare variable $database:max-db := "max";


(:~
 : @return doc in max db
 :)
declare function database:get-document($documentPath as xs:string) as document-node()?{
    try {
	 db:get($database:max-db, $documentPath)
	}
	catch db:open {
	    fn:error(xs:QName('err:max-database'), 'max database does not exist !')
	}

};


(:~
 : @return identified fragment in a document of max db
 :)
declare function database:get-element-by-id($documentPath, $id as xs:string) as element()?{
	database:get-document($documentPath)//*[@*:id=$id]
};


