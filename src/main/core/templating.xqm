xquery version "3.0";

(:~
: MaX "templating" module
:)
module namespace templating = "https://certic.unicaen.fr/max/templating";

import module namespace conf = 'https://certic.unicaen.fr/max/conf';
import module namespace i18n = 'https://certic.unicaen.fr/max/i18n';
import module namespace utils= 'https://certic.unicaen.fr/max/utils';

declare function templating:render($template as xs:string, $map as map(*)){
    let $filePath := templating:template-file($template)
    let $templateDoc := try {
        doc($filePath)
    }
    catch * {html:doc($filePath)}
    let $extends := $templateDoc//*:div/@*:data-extends
    let $render :=
        if($extends)
        then
            let $childRender := templating:eval-template($templateDoc, $map)
            let $parent := templating:eval-template(
            html:doc(templating:template-file( string($templateDoc//*:div/@data-extends))),
             $map)
            (: replace data-include:)
            let $withIncludes :=
                copy $c := $parent
                modify (
                  for $dataInclude in $c//*:div[@data-include]
                    let $templateName := string($dataInclude/@data-include)
                    let $templateDoc := doc(templating:template-file($templateName))
                    return replace node $dataInclude with templating:eval-template($templateDoc, $map)
                )
                return $c

             (: replace data-block :)
            let $withBlocks :=
                copy $c := $withIncludes
                modify (
                  for $dataBlock in $c//*:slot[@name]
                    let $blockName := string($dataBlock/@name)
                    return replace node $dataBlock with $childRender//*[@*:slot=$blockName]
                )
                return $c
            return $withBlocks
        else templating:eval-template($templateDoc, $map)

    return if(conf:is-dev-mode())
           then
            let $devLink :=<a
                style="position: fixed; bottom :5px; left: 5px; background-color: #333; padding: 2px 5px; color: white" href="/max-infos.html">?</a>
            return
                copy $c := $render
                modify (
                    insert node $devLink into $c//*:body
                )
                return $c
           else $render
};


declare function templating:eval-template($templateDoc as document-node(), $map as map(*)){
    let $params := map:put($map, 'active', fn:replace(fn:tokenize(request:path(),'/')[last()],'.html',''))
    let $declarations := string-join(
    for $var in map:keys($params)
                return "declare variable $" || $var || " external;")
    let $import := "import module namespace templating = 'https://certic.unicaen.fr/max/templating';"
    return xquery:eval($import ||$declarations ||  serialize($templateDoc), $params)
};

declare function templating:template-file($filename as xs:string){

    let $userFile := file:current-dir()|| 'templates/' || $filename
    return
        if(file:exists($userFile))
        then $userFile
        else
            let $voc := conf:get-vocabulary-bundle()
            let $bundleFile:= utils:get-bundles-path()  || $voc || '/templates/' || $filename
            return if(file:exists($bundleFile))
               then $bundleFile
               else fn:error(xs:QName('err:max-templating'),'no template found : ' || $userFile || ', ' || $bundleFile)

};

declare function templating:languageSelect($currentLang as xs:string?) as node()?{
    let $languages := conf:get-languages()

    return
    if(count($languages) > 1)
        then
            let $routeSuffix := substring(request:path(), 4)
            return
                    <ul class="languages navbar-nav">
                    { for $l in $languages
                        return
                            <li class="nav-item"><a class="nav-link p-2 ms-1 me-1 {if($currentLang = $l) then 'active' else ''}"
                            href="/{$l || $routeSuffix}">{$l}
                            </a></li>
                    }
                    </ul>
        else ()
};


(:~
 : translation
 : @param $key string to translate
 : @param $lang language
 : @return string translated string
 :)
declare function templating:translate($key as xs:string, $lang as xs:string) as xs:string {
    i18n:translate($key, $lang)
};

(:~
 : project title
 : @return string read from config file
 :)
declare function templating:get-title() as xs:string {
    conf:get-title()
};

(:~
 : bundle static file url
 : @return string
 :)
declare function templating:static($bundleName as xs:string, $filename as xs:string) as xs:string{
    '/' || $bundleName || '/static/' || $filename
};

